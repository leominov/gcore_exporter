package main

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

type Config struct {
	LogLevel string `envconfig:"LOG_LEVEL" default:"info"`
	Gcore    struct {
		APIToken string   `envconfig:"API_TOKEN" required:"true"`
		Debug    bool     `envconfig:"DEBUG"`
		Endpoint string   `envconfig:"ENDPOINT" default:"https://api.gcdn.co/" required:"true"`
		Metrics  []string `envconfig:"METRICS" default:"total_bytes,sent_bytes,requests,responses_hit,responses_miss"`
	} `envconfig:"GCORE"`
	Cache struct {
		CleanupInterval   time.Duration `envconfig:"CLEANUP_INTERVAL" default:"10m"`
		Expiration        time.Duration `envconfig:"EXPIRATION" default:"60m"`
		ExpirationStagger time.Duration `envconfig:"EXPIRATION_STAGGER" default:"10m"`
	} `envconfig:"CACHE"`
}

func LoadConfig() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("", c)
	if err != nil {
		return nil, err
	}
	level, err := logrus.ParseLevel(c.LogLevel)
	if err != nil {
		return nil, err
	}
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetLevel(level)
	return c, err
}

func (c *Config) String(pretty ...bool) string {
	var b []byte
	if len(pretty) > 0 && pretty[0] {
		b, _ = json.MarshalIndent(c, "", "   ")
	} else {
		b, _ = json.Marshal(c)
	}
	body := string(b)
	if len(c.Gcore.APIToken) > 0 {
		body = strings.Replace(body, c.Gcore.APIToken, "***", -1)
	}
	return body
}
