package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestRandomStagger(t *testing.T) {
	assert.Zero(t, RandomStagger(0))
	assert.NotZero(t, RandomStagger(time.Second))
}

func TestSeedMathRand(t *testing.T) {
	SeededSecurely = false
	SeedMathRand()
	assert.True(t, SeededSecurely)

	SeededSecurely = false
	SeedMathRand()
	assert.False(t, SeededSecurely)
}
