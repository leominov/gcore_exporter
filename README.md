# G-Core Exporter

[![Build Status](https://travis-ci.com/leominov/gcore_exporter.svg?branch=master)](https://travis-ci.com/leominov/gcore_exporter)

## Environment variables

* `CACHE_CLEANUP_INTERVAL` (default `10m`)
* `CACHE_EXPIRATION_STAGGER` (default `10m`)
* `CACHE_EXPIRATION` (default `60m`)
* `GCORE_API_TOKEN`
* `GCORE_DEBUG` (default `false`)
* `GCORE_ENDPOINT` (default `https://api.gcdn.co/`)
* `GCORE_METRICS` (default `total_bytes,sent_bytes,requests,responses_hit,responses_miss`)
* `LOG_LEVEL` (default `info`)

## Configuration

```yaml
scrape_configs:
- job_name: advert-gcore
  scrape_interval: 60s
  metrics_path: /resource
  params:
    granularity: [1m] # Duration of time chunks the data will be divided into (1m, 5m, 15m, 1h, 1d).
  static_configs:
    - targets:
        - "6171" # CDN Resource's id (ref: https://cdn.gcorelabs.com/resources/list)
      labels:
        group: advert-infra
        service: gcore
        cname: acdn.tinkoff.ru
        env: prod
  relabel_configs:
    - source_labels: [__address__]
      regex: (.*)
      target_label: __param_target
    - source_labels: []
      target_label: __address__
      replacement: 127.0.0.1:8080 # The gcore exporter's real hostname:port.
```

## Metrics

### Global

* `gcore_exporter_api_failed_requests` by (path, code)
* `gcore_exporter_cache_hits_total`
* `gcore_exporter_cache_items_count`
* `gcore_exporter_cache_misses_total`

### Target

* `gcore_exporter_request_success`
* `gcore_exporter_resource_certificate_info` by (name, deleted)
* `gcore_exporter_resource_info` by (status, cname, deleted, enabled)
* `gcore_exporter_resource_stats` by (metrics)

## Links

- https://hub.docker.com/repository/docker/leominov/gcore_exporter
- https://github.com/leominov/gcore_exporter/releases
- https://gcorelabs.com
- https://gcorelabs.com/blog/permanent-api-token-explained/
- https://apidocs.gcorelabs.com
