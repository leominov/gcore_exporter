package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

var (
	Version = "0.0.0"

	listenAddress = flag.String("web.listen-address", "0.0.0.0:8080", "Address to listen on for web interface and telemetry.")
	metricsPath   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	printsVersion = flag.Bool("version", false, "Prints version and exit.")
	printsConfig  = flag.Bool("config", false, "Prints configuration and exit.")
)

func main() {
	flag.Parse()

	if *printsVersion {
		fmt.Println(Version)
		return
	}

	// Hint message
	if !*printsConfig && len(os.Getenv("GCORE_API_TOKEN")) == 0 && len(os.Getenv("GCORE_PASSWORD")) > 0 {
		logrus.Error("Use GCORE_API_TOKEN instead GCORE_PASSWORD and GCORE_USERNAME")
		logrus.Error("More info: https://gcorelabs.com/blog/permanent-api-token-explained/")
	}

	config, err := LoadConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	if *printsConfig {
		fmt.Println(config.String(true))
		os.Exit(0)
	}

	e := NewExporter(config)
	prometheus.MustRegister(e)

	logrus.Infof("Starting gcore_exporter %s...", Version)
	logrus.Debugf("Configuration: %s", config.String())

	logrus.Infof("Listening on address: %s", *listenAddress)

	http.Handle(*metricsPath, promhttp.Handler())
	http.HandleFunc("/resource", e.ResourceMetricsRequest)
	http.HandleFunc("/-/flush", e.CacheFlushRequest)
	http.HandleFunc("/-/healthy", e.HealthyRequest)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>Gcore Exporter</title></head>
             <body>
             <h1>Gcore Exporter</h1>
				 <ul>
					<li><a href='` + *metricsPath + `'>Metrics</a></li>
					<li><a href='https://github.com/leominov/gcore_exporter/blob/master/README.md'>Docs</a></li>
				 </ul>
             </body>
             </html>`))
	})

	go func() {
		if err := http.ListenAndServe(*listenAddress, nil); err != nil {
			logrus.WithError(err).Fatal("Error starting HTTP server")
		}
	}()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	s := <-signalChan

	logrus.Printf("Captured %v. Exiting...", s)
}
