package main

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	gocache "github.com/patrickmn/go-cache"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"

	"github.com/leominov/gcore_exporter/gcore"
)

const namespace = "gcore_exporter"

type Exporter struct {
	cache  *gocache.Cache
	client *gcore.Client
	c      *Config
}

func NewExporter(c *Config) *Exporter {
	client := gcore.NewClient(
		gcore.WithEndpoint(c.Gcore.Endpoint),
		gcore.WithDebug(c.Gcore.Debug),
		gcore.WithAPIToken(c.Gcore.APIToken),
	)
	cache := gocache.New(
		c.Cache.Expiration,
		c.Cache.CleanupInterval,
	)
	return &Exporter{
		cache:  cache,
		client: client,
		c:      c,
	}
}

func (e *Exporter) getCachedResourceInfo(id int) (*gcore.Resource, error) {
	resKey := fmt.Sprintf("resource/%d", id)
	if resRaw, ok := e.cache.Get(resKey); ok {
		cacheHitsTotal.Inc()
		return resRaw.(*gcore.Resource), nil
	}
	cacheMissesTotal.Inc()
	resource, _, err := e.client.Resources.Get(id)
	if err != nil {
		return nil, err
	}
	d := e.c.Cache.Expiration + RandomStagger(e.c.Cache.ExpirationStagger)
	err = e.cache.Add(resKey, resource, d)
	if err != nil {
		logrus.WithError(err).Warn("Failed to update cache")
	}
	return resource, nil
}

func (e *Exporter) getCachedSSLCertificate(id int) (*gcore.SSLCertificate, error) {
	certKey := fmt.Sprintf("certificate/%d", id)
	if certRaw, ok := e.cache.Get(certKey); ok {
		cacheHitsTotal.Inc()
		return certRaw.(*gcore.SSLCertificate), nil
	}
	cacheMissesTotal.Inc()
	cert, _, err := e.client.SSLCertificates.GetCertificate(id)
	if err != nil {
		return nil, err
	}
	d := e.c.Cache.Expiration + RandomStagger(e.c.Cache.ExpirationStagger)
	err = e.cache.Add(certKey, cert, d)
	if err != nil {
		logrus.WithError(err).Warn("Failed to update cache")
	}
	return cert, nil
}

func (e *Exporter) collectResourceStats(registry *prometheus.Registry, resource *gcore.Resource, granularity gcore.Granularity) error {
	now := time.Now().Truncate(time.Minute)

	opts := &gcore.ResourceStatisticsOptions{
		Service:     "CDN",
		From:        now.Add(-2 * time.Minute),
		To:          now.Add(-time.Minute),
		Granularity: granularity,
		Resource: []int{
			resource.ID,
		},
		GroupBy: gcore.GroupByResource,
		Metrics: e.c.Gcore.Metrics,
	}

	stats, _, _, err := e.client.Statistics.GetResourceStatistics(opts)
	if err != nil {
		if err.Error() == "EOF" {
			return nil
		}
		return err
	}

	resourceStatsGaugeVec := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "resource_stats",
		Help:      "Resource statistics",
	}, []string{"metrics"})

	registry.MustRegister(resourceStatsGaugeVec)

	for _, stat := range stats.Resource {
		for metrics, dataAr := range stat.Metrics {
			for _, data := range dataAr {
				val := data[len(data)-1]
				resourceStatsGaugeVec.WithLabelValues(metrics).Set(float64(val))
			}
		}
	}

	return nil
}

func (e *Exporter) ResourceMetricsRequest(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	target := params.Get("target")

	resourceID, err := strconv.Atoi(target)
	if err != nil {
		logrus.WithField("params", params).WithError(err).Error("Incorrect resource ID")
		http.Error(w, "Incorrect resource ID", http.StatusBadRequest)
		return
	}

	granularityRaw := params.Get("granularity")
	granularity, err := gcore.ParseGranularity(granularityRaw)
	if err != nil {
		logrus.WithError(err).Debug("Default value will be used")
	}

	registry := prometheus.NewRegistry()

	requestSuccessGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "request_success",
		Help:      "Displays whether or not the request was a success",
	})

	resourceInfoGaugeVec := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "resource_info",
		Help:      "Information about resource",
	}, []string{"status", "cname", "deleted", "enabled"})

	resourceSSLCertificateInfoGaugeVec := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "resource_certificate_info",
		Help:      "Information about resource certificate",
	}, []string{"name", "issuer", "deleted"})

	registry.MustRegister(requestSuccessGauge)
	registry.MustRegister(resourceInfoGaugeVec)
	registry.MustRegister(resourceSSLCertificateInfoGaugeVec)

	resource, err := e.getCachedResourceInfo(resourceID)
	if err != nil {
		HandleRequestFailure(err)
		logrus.WithError(err).WithField("id", target).
			Error("Failed to collect resource info")

		requestSuccessGauge.Set(0)

		h := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})
		h.ServeHTTP(w, r)
		return
	}

	requestSuccessGauge.Set(1)
	resourceInfoGaugeVec.WithLabelValues(
		resource.Status,
		resource.CNAME,
		strconv.FormatBool(resource.Deleted),
		strconv.FormatBool(resource.Enabled),
	).Set(1)

	if resource.SSLData > 0 {
		cert, err := e.getCachedSSLCertificate(resource.SSLData)
		if err != nil {
			HandleRequestFailure(err)
			logrus.WithError(err).WithField("sslData", resource.SSLData).
				Error("Failed to get certificate")
		} else {
			resourceSSLCertificateInfoGaugeVec.WithLabelValues(
				cert.Name,
				cert.CertIssuer,
				strconv.FormatBool(cert.Deleted),
			).Set(float64(cert.ValidityNotAfter.Unix()))
		}
	}

	err = e.collectResourceStats(registry, resource, granularity)
	if err != nil {
		HandleRequestFailure(err)
		logrus.WithError(err).WithField("id", target).
			Error("Failed to collect resource stats")
	}

	h := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})
	h.ServeHTTP(w, r)
}

func (e *Exporter) CacheFlushRequest(w http.ResponseWriter, r *http.Request) {
	e.cache.Flush()
	w.WriteHeader(http.StatusNoContent)
}

func (e *Exporter) HealthyRequest(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("ok"))
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- cachedItems.Desc()
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	cachedItems.Set(float64(e.cache.ItemCount()))
	ch <- cachedItems
}
