package gcore

import "encoding/json"

type StringSlice []string

func (s *StringSlice) UnmarshalJSON(b []byte) error {
	var sliceType []string
	if err := json.Unmarshal(b, &sliceType); err == nil {
		*s = sliceType
		return nil
	}
	var stringType string
	if err := json.Unmarshal(b, &stringType); err != nil {
		return err
	}
	*s = []string{stringType}
	return nil
}
