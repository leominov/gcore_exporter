package gcore

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestToken_IsExpired(t *testing.T) {
	tests := []struct {
		t *Token
		e bool
	}{
		{
			t: &Token{},
			e: true,
		},
		{
			t: &Token{
				tokenExp: time.Now().Add(time.Minute),
			},
			e: false,
		},
		{
			t: &Token{
				tokenExp: time.Now().Add(-time.Minute),
			},
			e: true,
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.e, test.t.IsExpired())
	}
}

func TestToken_IsRefreshExpired(t *testing.T) {
	tests := []struct {
		t *Token
		e bool
	}{
		{
			t: &Token{},
			e: true,
		},
		{
			t: &Token{
				refreshExp: time.Now().Add(time.Minute),
			},
			e: false,
		},
		{
			t: &Token{
				refreshExp: time.Now().Add(-time.Minute),
			},
			e: true,
		},
	}
	for _, test := range tests {
		assert.Equal(t, test.e, test.t.IsRefreshExpired())
	}
}
