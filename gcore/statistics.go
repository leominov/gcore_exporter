package gcore

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/google/go-querystring/query"
)

const (
	Granularity1m  Granularity = "1m"
	Granularity5m  Granularity = "5m"
	Granularity15m Granularity = "15m"
	Granularity1h  Granularity = "1h"
	Granularity1d  Granularity = "1d"

	GroupByResource GroupBy = "resource"
	GroupByRegion   GroupBy = "region"
	GroupByVHost    GroupBy = "vhost"
)

var (
	granularityMap = map[string]Granularity{
		"1m":  Granularity1m,
		"5m":  Granularity5m,
		"15m": Granularity15m,
		"1h":  Granularity1h,
		"1d":  Granularity1d,
	}
)

type GroupBy string

type Granularity string

type Statistics struct {
	c *Client
}

type ResourceStatisticsOptions struct {
	Service     string      `url:"service"`
	From        time.Time   `url:"from"`
	To          time.Time   `url:"to"`
	Granularity Granularity `url:"granularity"`
	Resource    []int       `url:"resource"`
	// Only single value for now
	GroupBy GroupBy  `url:"group_by"`
	Metrics []string `url:"metrics"`
}

type ResourceStatistics struct {
	Resource map[string]*ResourceStatisticsGroup `json:"resource"`
	Region   map[string]*ResourceStatisticsGroup `json:"region"`
	VHost    map[string]*ResourceStatisticsGroup `json:"vhost"`
}

type ResourceStatisticsGroup struct {
	Metrics map[string][][]int64 `json:"metrics"`
}

type ShieldUsage struct {
	ActiveFrom time.Time `json:"active_from"`
	ActiveTo   time.Time `json:"active_to"`
	ClientID   int       `json:"client_id"`
	CNAME      string    `json:"cname"`
}

// Get CDN statistics for up to 90 days from today. Required parameters are marked with the asterisk *.
// https://apidocs.gcorelabs.com/cdn#tag/Statistics/paths/~1statistics~1series/get
func (s *Statistics) GetResourceStatistics(opts *ResourceStatisticsOptions) (*ResourceStatistics, []*ShieldUsage, *Response, error) {
	v, err := query.Values(opts)
	if err != nil {
		return nil, nil, nil, err
	}
	req, err := s.c.NewRequest(http.MethodGet, "statistics/series?"+v.Encode(), nil)
	if err != nil {
		return nil, nil, nil, err
	}
	if opts != nil && len(opts.Metrics) == 1 && opts.Metrics[0] == "shield_usage" {
		var res []*ShieldUsage
		resp, err := s.c.doRequest(req, &res)
		return nil, res, resp, err
	}
	res := &ResourceStatistics{}
	resp, err := s.c.doRequest(req, &res)
	return res, nil, resp, err
}

func ParseGranularity(name string) (Granularity, error) {
	v, ok := granularityMap[strings.ToLower(name)]
	if !ok {
		return Granularity1m, fmt.Errorf("unknown granularity: %s", name)
	}
	return v, nil
}
