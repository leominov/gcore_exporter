package gcore

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAPIError_Error(t *testing.T) {
	tests := []struct {
		json  string
		error string
	}{
		{
			json:  `{"error": {"error": "Error occurred."}}`,
			error: "Error occurred.",
		},
		{
			json:  `{"error": {"message": "JSON parse error..."}}`,
			error: "JSON parse error...",
		},
		{
			json:  `{"error": {"errors": {"errors": "No active account found with the given credentials."}}}`,
			error: "errors: No active account found with the given credentials.",
		},
		{
			json:  `{"error": {"errors": {"errors": ["No active account found with the given credentials."]}}}`,
			error: "errors: No active account found with the given credentials.",
		},
		{
			json:  `{"error": {}}`,
			error: "Unexpected error",
		},
	}
	for _, test := range tests {
		testStruct := map[string]APIError{}
		err := json.Unmarshal([]byte(test.json), &testStruct)
		assert.NoError(t, err)
		e, ok := testStruct["error"]
		if assert.True(t, ok) {
			assert.Equal(t, test.error, e.Error())
		}
	}
	err := APIError{
		responseCode: 404,
	}
	assert.Equal(t, "Not Found. Requested item doesn't exist", err.Error())
	err = APIError{
		responseCode: 504,
	}
	assert.Equal(t, "Unexpected response code: 504", err.Error())
}

func TestAPIError_RequestPath(t *testing.T) {
	err := APIError{}
	assert.Equal(t, "", err.RequestPath())
	err.requestPath = "/foobar"
	assert.Equal(t, "/foobar", err.RequestPath())
}

func TestAPIError_ResponseCode(t *testing.T) {
	err := APIError{}
	assert.Equal(t, 0, err.ResponseCode())
	err.responseCode = 404
	assert.Equal(t, 404, err.ResponseCode())
}
