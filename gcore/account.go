package gcore

import (
	"fmt"
	"net/http"
	"time"
)

const (
	tokenTTL   = time.Hour
	refreshTTL = 24 * time.Hour
)

type Account struct {
	c *Client
}

type LoginOptions struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type AccountDetails struct {
	ID               int                      `json:"id"`
	Deleted          bool                     `json:"deleted"`
	Email            string                   `json:"email"`
	Phone            string                   `json:"phone"`
	Name             string                   `json:"name"`
	CompanyName      string                   `json:"companyName"`
	Website          string                   `json:"website"`
	UtilizationLevel int                      `json:"utilization_level"`
	UseBalancer      bool                     `json:"use_balancer"`
	BillType         string                   `json:"bill_type"`
	CNAME            string                   `json:"cname"`
	BlogAgree        bool                     `json:"blog_agree"`
	CurrentUser      int                      `json:"currentUser"`
	Capabilities     []string                 `json:"capabilities"`
	Status           string                   `json:"status"`
	ServiceStatuses  map[string]ServiceStatus `json:"serviceStatuses"`
	PaidFeatures     map[string][]PaidFeature `json:"paidFeatures"`
	FreeFeatures     map[string][]FreeFeature `json:"freeFeatures"`
	EntryBaseDomain  string                   `json:"entryBaseDomain"`
	IntercomHmac     string                   `json:"intercom_hmac"`
	Users            []*AccountUser           `json:"users"`
	Groups           []*AccountGroup          `json:"groups"`
	SSLDatas         []*AccountSSLData        `json:"sslDatas"`
	IsTest           bool                     `json:"is_test"`
}

type BaseFeature struct {
	FeatureID  int       `json:"feature_id"`
	Service    string    `json:"service"`
	Name       string    `json:"name"`
	CreateDate time.Time `json:"create_date"`
}

type PaidFeature struct {
	BaseFeature
	PaidFeatureID int `json:"paid_feature_id"`
}

type FreeFeature struct {
	BaseFeature
	PaidFeatureID int `json:"paid_feature_id"`
}

type ServiceStatus struct {
	Status  string `json:"status"`
	Enabled bool   `json:"enabled"`
}

type AccountSSLData struct {
	ID                  int       `json:"id"`
	Automated           bool      `json:"automated"`
	Deleted             bool      `json:"deleted"`
	CertIssuer          string    `json:"cert_issuer"`
	CertSubjectCN       string    `json:"cert_subject_cn"`
	CertSubjectAlt      string    `json:"cert_subject_alt"`
	ValidityNotBefore   time.Time `json:"validity_not_before"`
	ValidityNotAfter    time.Time `json:"validity_not_after"`
	HasRelatedResources bool      `json:"hasRelatedResources"`
	Name                string    `json:"name"`
	SSLCertificateChain string    `json:"sslCertificateChain"`
}

type AccountUser struct {
	ID        int             `json:"id"`
	Deleted   bool            `json:"deleted"`
	Activated bool            `json:"activated"`
	Email     string          `json:"email"`
	Name      string          `json:"name"`
	Client    int             `json:"client"`
	Company   string          `json:"company"`
	Lang      string          `json:"lang"`
	Phone     string          `json:"phone"`
	SSOAuth   bool            `json:"sso_auth"`
	Groups    []*AccountGroup `json:"groups"`
}

type AccountGroup struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// https://apidocs.gcorelabs.com/cdn#tag/Account/paths/~1auth~1jwt~1login/post
// https://apidocs.gcorelabs.com/cdn#tag/Account/paths/~1auth~1jwt~1refresh/post
func (a *Account) Login(opts *LoginOptions) (*Response, error) {
	if a.c.token == nil || (a.c.token != nil && a.c.token.IsRefreshExpired()) {
		return a.doLoginOrRefresh("auth/jwt/login", opts)
	}
	if !a.c.token.IsExpired() {
		return nil, nil
	}
	// Requesting refresh method with expired token returns 401 after 9 Feb. 2021
	a.c.token.Access = ""
	res, err := a.doLoginOrRefresh("auth/jwt/refresh", map[string]string{
		"refresh": a.c.token.Refresh,
	})
	if err != nil {
		// In case of failed refreshing just reset expiration time
		// to get new token
		a.c.token = nil
		return nil, fmt.Errorf("failed to refresh: %v", err)
	}
	return res, err
}

func (a *Account) doLoginOrRefresh(path string, opts interface{}) (*Response, error) {
	issues := time.Now()
	req, err := a.c.NewRequest(http.MethodPost, path, opts)
	if err != nil {
		return nil, err
	}
	token := &Token{
		refreshExp: issues.Add(refreshTTL),
		tokenExp:   issues.Add(tokenTTL),
	}
	// TODO(l.aminov): Switch to JWT
	resp, err := a.c.doRequest(req, token)
	if err != nil {
		return resp, err
	}
	a.c.token = token
	return resp, err
}

// Get information about your profile, users and other account details
// https://apidocs.gcorelabs.com/cdn#tag/Account/paths/~1clients~1me/get
func (a *Account) GetAccountDetails() (*AccountDetails, *Response, error) {
	req, err := a.c.NewRequest(http.MethodGet, "clients/me", nil)
	if err != nil {
		return nil, nil, err
	}
	details := &AccountDetails{}
	resp, err := a.c.doRequest(req, details)
	return details, resp, err
}
