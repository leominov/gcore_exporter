package gcore

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSSLCertificates_GetCertificate(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/sslData/1":
			w.Write([]byte(`{
"name": "Some certifacate",
"deleted": false,
"cert_issuer": "Some certification center",
"cert_subjeck_cn": "mydomain.name",
"validity_not_before": "2016-09-17T18:06:01Z",
"validity_not_after": "2017-08-21T11:02:00Z",
"hasRelatedResources": true,
"automated": false
}`))
		case "/sslData/2":
			w.Write([]byte(`{"name": true}`))
		case "/sslData/3":
			w.WriteHeader(http.StatusInternalServerError)
		}
	}))
	defer server.Close()

	cli := NewClient(WithEndpoint(server.URL), WithDebug(true))
	cert, _, err := cli.SSLCertificates.GetCertificate(1)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, "Some certifacate", cert.Name)

	_, _, err = cli.SSLCertificates.GetCertificate(2)
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "cannot unmarshal")
	}

	_, _, err = cli.SSLCertificates.GetCertificate(3)
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "Unexpected response code: 500")
	}
}
