package gcore

import (
	"fmt"
	"net/http"
	"time"
)

type SSLCertificates struct {
	c *Client
}

type SSLCertificate struct {
	ID                  int       `json:"id"`
	Name                string    `json:"name"`
	Deleted             bool      `json:"deleted"`
	CertIssuer          string    `json:"cert_issuer"`
	CertSubjectCN       string    `json:"cert_subject_cn"`
	CertSubjectAlt      string    `json:"cert_subject_alt"`
	ValidityNotBefore   time.Time `json:"validity_not_before"`
	ValidityNotAfter    time.Time `json:"validity_not_after"`
	HasRelatedResources bool      `json:"hasRelatedResources"`
	Automated           bool      `json:"automated"`
}

func (s *SSLCertificates) GetCertificate(id int) (*SSLCertificate, *Response, error) {
	req, err := s.c.NewRequest(http.MethodGet, fmt.Sprintf("/sslData/%d", id), nil)
	if err != nil {
		return nil, nil, err
	}
	cert := &SSLCertificate{}
	resp, err := s.c.doRequest(req, cert)
	return cert, resp, err
}
