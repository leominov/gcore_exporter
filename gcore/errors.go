package gcore

import (
	"fmt"
	"strings"
)

var (
	responseCodeToStringMap = map[int]string{
		400: "Bad Request. Required parameter is missing",
		401: "Unauthorized. Provided credentials are invalid or your API token has expired",
		403: "Forbidden. Access denied. You do not have enough rights",
		404: "Not Found. Requested item doesn't exist",
		// 4 RPS per IP
		429: "Too Many Requests. You have exceeded number of allowed requests for your resources",
		503: "Service Unavailable. You have made a mistake in request parameters or service is currently unavailable",
	}
)

type APIError struct {
	Err     string                 `json:"error"`
	Message string                 `json:"message"`
	Errors  map[string]StringSlice `json:"errors"`

	requestPath  string
	responseCode int
}

func (a APIError) Error() string {
	if len(a.Err) > 0 {
		return a.Err
	}
	if len(a.Message) > 0 {
		return a.Message
	}
	if a.Errors == nil {
		if a.responseCode > 299 {
			if desc, ok := responseCodeToStringMap[a.responseCode]; ok {
				return desc
			}
			return fmt.Sprintf("Unexpected response code: %d", a.responseCode)
		}
		return "Unexpected error"
	}
	var errs []string
	for k, v := range a.Errors {
		errs = append(errs, fmt.Sprintf("%s: %s", k, strings.Join(v, ", ")))
	}
	return strings.Join(errs, "; ")
}

func (a APIError) RequestPath() string {
	return a.requestPath
}

func (a APIError) ResponseCode() int {
	return a.responseCode
}
