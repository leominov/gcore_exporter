package gcore

import "time"

type Token struct {
	Refresh    string `json:"refresh"`
	Access     string `json:"access"`
	refreshExp time.Time
	tokenExp   time.Time
}

func (t *Token) IsExpired() bool {
	if t.tokenExp.IsZero() {
		return true
	}
	return t.tokenExp.Before(time.Now())
}

func (t *Token) IsRefreshExpired() bool {
	if t.refreshExp.IsZero() {
		return true
	}
	return t.refreshExp.Before(time.Now())
}
