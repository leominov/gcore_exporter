package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/leominov/gcore_exporter/gcore"
)

var (
	debug = flag.Bool("debug", false, "Enable debug logs")
	id    = flag.Int("id", 0, "Resource ID")
)

func main() {
	flag.Parse()

	cli := gcore.NewClient(gcore.WithDebug(*debug))
	_, err := cli.Account.Login(&gcore.LoginOptions{
		Username: os.Getenv("GCORE_USERNAME"),
		Password: os.Getenv("GCORE_PASSWORD"),
	})
	if err != nil {
		log.Fatal(err)
	}

	resource, _, err := cli.Resources.Get(*id)
	if err != nil {
		log.Fatal(err)
	}

	b, err := json.MarshalIndent(resource, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(b))
}
