package gcore

import (
	"fmt"
	"net/http"
	"time"
)

type Resources struct {
	c *Client
}

type ResourceListItem struct {
	ID                 int       `json:"id"`
	Deleted            bool      `json:"deleted"`
	Enabled            bool      `json:"enabled"`
	CompanyName        string    `json:"companyName"`
	Status             string    `json:"status"`
	Client             int       `json:"client"`
	OriginGroup        int       `json:"originGroup"`
	Origin             string    `json:"origin"`
	CNAME              string    `json:"cname"`
	SecondaryHostnames []string  `json:"secondaryHostnames"`
	CreatedAt          time.Time `json:"createdAt"`
	UpdatedAt          time.Time `json:"updatedAt"`
}

type Resource struct {
	ID                 int      `json:"id"`
	Deleted            bool     `json:"deleted"`
	Enabled            bool     `json:"enabled"`
	OriginGroup        int      `json:"originGroup"`
	Origin             string   `json:"origin"`
	SecondaryHostnames []string `json:"SecondaryHostnames"`
	SSLEnabled         bool     `json:"sslEnabled"`
	SSLData            int      `json:"sslData"`
	SSLAutomated       bool     `json:"ssl_automated"`
	OriginProtocol     string   `json:"originProtocol"`
	LogTarget          string   `json:"logTarget"`
	CNAME              string   `json:"cname"`
	Status             string   `json:"status"`
	Options            struct {
		BrowserCacheSettings    *BrowserCacheSettingsOption    `json:"browser_cache_settings"`
		EdgeCacheSettings       *EdgeCacheSettingsOption       `json:"edge_cache_settings"`
		DisableCache            *DisableCacheOption            `json:"disable_cache"`
		CacheHTTPHeaders        *CacheHTTPHeadersOption        `json:"cache_http_headers"`
		IgnoreCookie            *IgnoreCookieOption            `json:"ignore_cookie"`
		IgnoreQueryString       *IgnoreQueryStringOption       `json:"ignoreQueryString"`
		QueryParamsWhitelist    *QueryParamsWhitelistOption    `json:"query_params_whitelist"`
		QueryParamsBlacklist    *QueryParamsBlacklistOption    `json:"query_params_blacklist"`
		Slice                   *SliceOption                   `json:"slice"`
		FetchCompressed         *FetchCompressedOption         `json:"fetch_compressed"`
		GzipOn                  *GzipOnOption                  `json:"gzipOn"`
		BrotliCompression       *BrotliCompressionOption       `json:"brotli_compression"`
		RedirectHTTPtoHTTPS     *RedirectHTTPtoHTTPSOption     `json:"redirect_http_to_https"`
		RedirectHTTPStoHTTP     *RedirectHTTPStoHTTPOption     `json:"redirect_https_to_http"`
		HostHeader              *HostHeaderOption              `json:"hostHeader"`
		ForwardHostHeader       *ForwardHostHeaderOption       `json:"forward_host_header"`
		StaticHeaders           *StaticHeadersOption           `json:"staticHeaders"`
		CORS                    *CORSOption                    `json:"cors"`
		Stale                   *StaleOption                   `json:"stale"`
		AllowedHTTPMethods      *AllowedHTTPMethodsOption      `json:"allowedHttpMethods"`
		ProxyCacheMethodsSet    *ProxyCacheMethodsSetOption    `json:"proxy_cache_methods_set"`
		DisableProxyForceRanges *DisableProxyForceRangesOption `json:"disable_proxy_force_ranges"`
		StaticRequestHeaders    *StaticRequestHeadersOption    `json:"staticRequestHeaders"`
		CustomServerName        *CustomServerNameOption        `json:"custom_server_name"`
		Rewrite                 *RewriteOption                 `json:"rewrite"`
		ForceReturn             *ForceReturnOption             `json:"force_return"`
		CountryACL              *CountryACLOption              `json:"country_acl"`
		UserAgentACL            *UserAgentACLOption            `json:"user_agent_acl"`
		IPAddressACL            *IPAddressACLOption            `json:"ip_address_acl"`
		ReferrerACL             *ReferrerACLOption             `json:"referrer_acl"`
		SecureKey               *SecureKeyOption               `json:"secure_key"`
	} `json:"options"`
}

type BrowserCacheSettingsOption struct {
	ResourceOptionBase
	Value string `json:"value"`
}

type EdgeCacheSettingsOption struct {
	ResourceOptionBase
	Value        string            `json:"value"`
	CustomValues map[string]string `json:"custom_values"`
	Default      string            `json:"default"`
}

type DisableCacheOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type CacheHTTPHeadersOption struct {
	ResourceOptionBase
	Value []string `json:"value"`
}

type IgnoreCookieOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type IgnoreQueryStringOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type QueryParamsWhitelistOption struct {
	ResourceOptionBase
	Value []string `json:"value"`
}

type QueryParamsBlacklistOption struct {
	ResourceOptionBase
	Value []string `json:"value"`
}

type SliceOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type FetchCompressedOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type GzipOnOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type BrotliCompressionOption struct {
	ResourceOptionBase
	Value []string `json:"value"`
}

type RedirectHTTPtoHTTPSOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type RedirectHTTPStoHTTPOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type HostHeaderOption struct {
	ResourceOptionBase
	Value string `json:"value"`
}

type ForwardHostHeaderOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type StaticHeadersOption struct {
	ResourceOptionBase
	Value map[string]StringSlice `json:"value"`
}

type CORSOption struct {
	ResourceOptionBase
	Value []string `json:"value"`
}

type StaleOption struct {
	ResourceOptionBase
	Value []string `json:"value"`
}

type AllowedHTTPMethodsOption struct {
	ResourceOptionBase
	Value []string `json:"value"`
}

type ProxyCacheMethodsSetOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type DisableProxyForceRangesOption struct {
	ResourceOptionBase
	Value bool `json:"value"`
}

type StaticRequestHeadersOption struct {
	ResourceOptionBase
	Value map[string]string `json:"value"`
}

type CustomServerNameOption struct {
	ResourceOptionBase
	Value string `json:"value"`
}

type RewriteOption struct {
	ResourceOptionBase
	Flag string `json:"flag"`
	Body string `json:"body"`
}

type ForceReturnOption struct {
	ResourceOptionBase
	Code int    `json:"code"`
	Body string `json:"body"`
}

type CountryACLOption struct {
	ResourceOptionBase
	PolicyType     string   `json:"policy_type"`
	ExceptedValues []string `json:"excepted_values"`
}

type UserAgentACLOption struct {
	ResourceOptionBase
	PolicyType     string   `json:"policy_type"`
	ExceptedValues []string `json:"excepted_values"`
}

type IPAddressACLOption struct {
	ResourceOptionBase
	PolicyType     string   `json:"policy_type"`
	ExceptedValues []string `json:"excepted_values"`
}

type ReferrerACLOption struct {
	ResourceOptionBase
	PolicyType     string   `json:"policy_type"`
	ExceptedValues []string `json:"excepted_values"`
}

type SecureKeyOption struct {
	ResourceOptionBase
	Key  string `json:"key"`
	Type int    `json:"type"`
}

type ResourceOptionBase struct {
	Enabled bool `json:"enabled"`
	Deleted bool `json:"deleted"`
}

// Get information about all CDN Resources in your account
// https://apidocs.gcorelabs.com/cdn#tag/Resources/paths/~1resources/get
func (r *Resources) GetAll() ([]*ResourceListItem, *Response, error) {
	req, err := r.c.NewRequest(http.MethodGet, "resources", nil)
	if err != nil {
		return nil, nil, err
	}
	var res []*ResourceListItem
	resp, err := r.c.doRequest(req, &res)
	return res, resp, err
}

// https://apidocs.gcorelabs.com/cdn#tag/Resources/paths/~1resources~1{resource_id}/get
func (r *Resources) Get(resourceID int) (*Resource, *Response, error) {
	req, err := r.c.NewRequest(http.MethodGet, fmt.Sprintf("resources/%d", resourceID), nil)
	if err != nil {
		return nil, nil, err
	}
	res := &Resource{}
	resp, err := r.c.doRequest(req, &res)
	return res, resp, err
}
