package gcore

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestResources_GetAll(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`[
  {
    "id": 220,
    "deleted": false,
    "active": true,
    "enabled": true,
    "companyName": "Your Company",
    "status": "active",
    "client": 170,
    "originGroup": 80,
    "origin": "Your site",
    "cname": "cdn.site.com",
    "secondaryHostnames": [
      "cdn1.yoursite.com",
      "cdn2.yoursite.com"
    ],
    "createdAt": "2017-06-10T10:30:04.954354Z",
    "updatedAt": "2017-06-14T05:05:42.065221Z"
  }
]`))
	}))
	defer server.Close()

	cli := NewClient(WithEndpoint(server.URL))
	items, _, err := cli.Resources.GetAll()
	if !assert.NoError(t, err) {
		return
	}
	if assert.Equal(t, 1, len(items)) {
		item := items[0]
		assert.Equal(t, 220, item.ID)
		assert.Equal(t, "active", item.Status)
		assert.Equal(t, "cdn.site.com", item.CNAME)
	}
}

func TestResources_Get(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`{
  "cname": "cdn.yoursite.com",
  "logTarget": "1.2.3.4:1234",
  "options": {
    "allowedHttpMethods": {
      "enabled": true,
      "value": [
        "GET",
        "POST"
      ]
    },
    "cors": {
      "enabled": true,
      "value": [
        "yourdomain.com",
        "yourdomain2.com"
      ]
    },
    "fetch_compressed": {
      "enabled": true,
      "value": false
    },
    "gzipOn": {
      "enabled": true,
      "value": true
    },
    "brotli_compression": {
      "enabled": true,
      "value": [
        "text/html",
        "text/plain"
      ]
    },
    "redirect_http_to_https": {
      "enabled": true,
      "value": true
    },
    "redirect_https_to_http": {
      "enabled": false,
      "value": true
    },
    "hostHeader": {
      "enabled": true,
      "value": "host.com"
    },
    "edge_cache_settings": {
      "enabled": true,
      "value": "43200s",
      "custom_values": {
        "100": "43200s"
      }
    },
    "browser_cache_settings": {
      "enabled": true,
      "value": "3600s"
    },
    "forward_host_header": {
      "enabled": false,
      "value": false
    },
    "disable_cache": {
      "enabled": true,
      "value": false
    },
    "cache_http_headers": {
      "enabled": false,
      "value": [
        "vary",
        "content-length",
        "last-modified",
        "connection",
        "accept-ranges",
        "content-type",
        "content-encoding",
        "etag",
        "cache-control",
        "expires",
        "keep-alive",
        "server"
      ]
    },
    "ignore_cookie": {
      "enabled": true,
      "value": true
    },
    "ignoreQueryString": {
      "enabled": true,
      "value": false
    },
    "query_params_whitelist": null,
    "query_params_blacklist": null,
    "proxy_cache_methods_set": {
      "enabled": true,
      "value": false
    },
    "disable_proxy_force_ranges": {
      "enabled": true,
      "value": true
    },
    "rewrite": {
      "body": "/(.*) /additional_path/$1",
      "enabled": true,
      "flag": "break"
    },
    "force_return": {
      "enabled": true,
      "code": 301,
      "body": "http://example.com/redirect_address"
    },
    "country_acl": {
      "enabled": true,
      "policy_type": "allow",
      "excepted_values": [
        "GB",
        "DE"
      ]
    },
    "referrer_acl": {
      "enabled": true,
      "policy_type": "deny",
      "excepted_values": [
        "google.com",
        "*.yandex.ru"
      ]
    },
    "user_agent_acl": {
      "enabled": true,
      "policy_type": "allow",
      "excepted_values": [
        "UserAgent Value"
      ]
    },
    "ip_address_acl": {
      "enabled": true,
      "policy_type": "deny",
      "excepted_values": [
        "192.168.1.100/32"
      ]
    },
    "secure_key": {
      "enabled": true,
      "key": "mysupersecretkey",
      "type": 2
    },
    "slice": null,
    "stale": {
      "enabled": true,
      "value": [
        "http_404",
        "http_500"
      ]
    },
    "staticHeaders": {
      "enabled": true,
      "value": {
        "X-Example": "Value_1",
        "X-Example-Multiple": [
          "Value_2",
          "Value_3"
        ]
      }
    },
    "staticRequestHeaders": {
      "enabled": true,
      "value": {
        "Header-One": "Value 1",
        "Header-Two": "Value 2"
      }
    },
    "sni": {
      "enabled": true,
      "value": {
        "sni_type": "custom",
        "custom_hostname": "custom.example.com"
      }
    }
  },
  "origin": "yoursite.com",
  "originProtocol": "HTTPS",
  "secondaryHostnames": [
    "cdn1.yoursite.com",
    "cdn2.yoursite.com"
  ],
  "sslData": 102,
  "sslEnabled": true,
  "proxy_ssl_enabled": false,
  "proxy_ssl_ca": null,
  "proxy_ssl_data": null
}`))
	}))
	defer server.Close()

	cli := NewClient(WithEndpoint(server.URL))
	item, _, err := cli.Resources.Get(1)
	if !assert.NoError(t, err) {
		return
	}
	if !assert.NotNil(t, item) {
		return
	}
	assert.Equal(t, 102, item.SSLData)
	assert.Equal(t, "cdn.yoursite.com", item.CNAME)
}
