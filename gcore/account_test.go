package gcore

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAccount_GetAccountDetails(t *testing.T) {
	server1 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`{
  "id": 0,
  "users": [
    {
      "id": 0,
      "email": "user@example.com",
      "name": "string",
      "lang": "ru",
      "phone": "string",
      "company": "string",
      "reseller": 0,
      "client": 0,
      "deleted": true,
      "groups": [
        {
          "id": 1,
          "name": "Administrators"
        }
      ],
      "activated": true,
      "sso_auth": true,
      "two_fa": true
    }
  ],
  "email": "user@example.com",
  "phone": "string",
  "name": "string",
  "status": "new",
  "companyName": "string",
  "website": "string",
  "sslDatas": [
    null
  ],
  "currentUser": 0,
  "capabilities": [
    "CDN"
  ],
  "serviceStatuses": {
    "CDN": {
	  "status": "new",
      "enabled": true
	},
    "CLOUD": {
	  "status": "new",
	  "enabled": true
	}
  },
  "paidFeatures": {
    "CDN": [{
      "feature_id": 1,
      "name": "paid feature name",
      "service": "CDN"
    }],
    "STREAMING": [{
      "feature_id": 2,
      "name": "another paid feature name",
      "service": "STREAMING"
    }]
  },
  "freeFeatures": {
    "CDN": [{
      "feature_id": 1,
      "name": "free feature name",
      "service": "CDN"
    }],
    "STREAMING": [{
      "feature_id": 2,
      "name": "another free feature name",
      "service": "STREAMING"
    }]
  },
  "entryBaseDomain": "string",
  "signup_process": "sign_up_full",
  "intercom_hmac": "string",
  "deleted": true,
  "logTarget": "string",
  "utilization_level": 0,
  "use_balancer": true,
  "bill_type": "string",
  "cname": "string",
  "blog_agree": true,
  "custom_id": "string",
  "country_code": "string",
  "is_test": true,
  "has_active_admin": true,
  "promotion": {}
}`))
	}))
	defer server1.Close()

	cli := NewClient(WithEndpoint(server1.URL))
	user, _, err := cli.Account.GetAccountDetails()
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, 0, user.ID)
	assert.Equal(t, "string", user.Name)
	assert.Equal(t, "user@example.com", user.Email)

	server2 := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`{"id": true}`))
	}))
	defer server2.Close()

	cli = NewClient(WithEndpoint(server2.URL))
	_, _, err = cli.Account.GetAccountDetails()
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "cannot unmarshal")
	}
}
