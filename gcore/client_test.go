package gcore

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewClient(t *testing.T) {
	cli := NewClient(
		WithToken(&Token{Access: "ABCD"}),
		WithDebug(true),
		WithEndpoint("https://google.com"),
	)
	assert.Equal(t, "ABCD", cli.GetToken().Access)
}
