package gcore

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseGranularity(t *testing.T) {
	for in, out := range granularityMap {
		pout, err := ParseGranularity(in)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, out, pout)
	}
	_, err := ParseGranularity("foobar")
	assert.Error(t, err)
}
