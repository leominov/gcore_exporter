package gcore

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"os"
	"strings"
)

const (
	defaultEndpoint  = "https://api.gcdn.co/"
	defaultUserAgent = "go-gcore-client/0.0.1 (+https://github.com/leominov/gcore_exporter)"
)

type ClientOption func(*Client)

type Client struct {
	UserAgent string

	Account         *Account
	Resources       *Resources
	SSLCertificates *SSLCertificates
	Statistics      *Statistics

	apiToken string
	debug    bool
	endpoint string
	httpCli  *http.Client
	token    *Token
}

func NewClient(options ...ClientOption) *Client {
	endpoint, ok := os.LookupEnv("GCORE_ENDPOINT")
	if !ok {
		endpoint = defaultEndpoint
	}
	c := &Client{
		UserAgent: defaultUserAgent,
		httpCli:   http.DefaultClient,
		endpoint:  endpoint,
	}
	c.Account = &Account{c}
	c.Statistics = &Statistics{c}
	c.Resources = &Resources{c}
	c.SSLCertificates = &SSLCertificates{c}
	for _, opt := range options {
		if opt == nil {
			continue
		}
		opt(c)
	}
	return c
}

func WithHTTPClient(httpCli *http.Client) ClientOption {
	return func(c *Client) {
		c.httpCli = httpCli
	}
}

func WithToken(token *Token) ClientOption {
	return func(c *Client) {
		c.token = token
	}
}

func WithEndpoint(endpoint string) ClientOption {
	return func(c *Client) {
		c.endpoint = endpoint
	}
}

func WithDebug(debug bool) ClientOption {
	return func(c *Client) {
		c.debug = debug
	}
}

func WithAPIToken(apiToken string) ClientOption {
	return func(c *Client) {
		c.apiToken = apiToken
	}
}

func (c *Client) NewRequest(method, urlStr string, body interface{}) (*http.Request, error) {
	u := joinURLPath(c.endpoint, urlStr)

	buf := new(bytes.Buffer)
	if body != nil {
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}

	req, err := http.NewRequest(method, u, buf)
	if err != nil {
		return nil, err
	}

	if len(c.apiToken) > 0 {
		req.Header.Set("Authorization", fmt.Sprintf("APIKey %s", c.apiToken))
	} else if c.token != nil && len(c.token.Access) > 0 {
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token.Access))
	}

	if len(c.UserAgent) > 0 {
		req.Header.Set("User-Agent", c.UserAgent)
	}

	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	if c.debug {
		out, err := httputil.DumpRequestOut(req, true)
		if err == nil {
			fmt.Println(string(out))
		}
	}

	return req, nil
}

func joinURLPath(endpoint string, path string) string {
	return strings.TrimRight(endpoint, "/") + "/" + strings.TrimLeft(path, "/")
}

type Response struct {
	*http.Response
}

func newResponse(r *http.Response) *Response {
	response := &Response{Response: r}
	return response
}

func (c *Client) GetToken() *Token {
	return c.token
}

func (c *Client) doRequest(req *http.Request, v interface{}) (*Response, error) {
	var (
		resp *http.Response
		err  error
	)

	resp, err = c.httpCli.Do(req)
	if err != nil {
		return nil, err
	}

	if c.debug {
		out, err := httputil.DumpResponse(resp, true)
		if err == nil {
			fmt.Println(string(out))
		}
	}

	defer resp.Body.Close()
	defer io.Copy(ioutil.Discard, resp.Body)

	response := newResponse(resp)

	err = checkResponse(resp)
	if err != nil {
		return response, err
	}

	if v != nil {
		if w, ok := v.(io.Writer); ok {
			io.Copy(w, resp.Body)
		} else {
			err = json.NewDecoder(resp.Body).Decode(v)
		}
	}

	return response, err
}

func checkResponse(r *http.Response) error {
	if c := r.StatusCode; 200 <= c && c <= 299 {
		return nil
	}

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	apiError := APIError{
		requestPath:  r.Request.URL.Path,
		responseCode: r.StatusCode,
	}
	errDec := json.Unmarshal(data, &apiError)
	if errDec != nil {
		apiError.Message = string(data)
	}

	return apiError
}
