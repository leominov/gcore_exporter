package gcore

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringSlice_UnmarshalJSON(t *testing.T) {
	tests := []struct {
		json    string
		wantErr bool
	}{
		{
			json:    `{"foo":"bar"}`,
			wantErr: false,
		},
		{
			json:    `{"foo":["bar"]}`,
			wantErr: false,
		},
		{
			json:    `{"foo":1}`,
			wantErr: true,
		},
		{
			json:    `{"foo":true}`,
			wantErr: true,
		},
	}
	for _, test := range tests {
		s := map[string]StringSlice{}
		err := json.Unmarshal([]byte(test.json), &s)
		if test.wantErr {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
		}
	}
}
