package main

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/leominov/gcore_exporter/gcore"
)

func TestHandleRequestFailure(t *testing.T) {
	err := errors.New("error")
	HandleRequestFailure(err)

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"error": "Error occurred."}`))
	}))
	defer server.Close()

	cli := gcore.NewClient(gcore.WithEndpoint(server.URL))
	_, err = cli.Account.Login(&gcore.LoginOptions{
		Username: "foo",
		Password: "bar",
	})
	if assert.Error(t, err) {
		HandleRequestFailure(err)
	}
}
