package main

import (
	"strconv"

	"github.com/prometheus/client_golang/prometheus"

	"github.com/leominov/gcore_exporter/gcore"
)

var (
	failedRequests = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "api_failed_requests",
		Help:      "Failed requests to API by path and code",
	}, []string{"path", "code"})
	cachedItems = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "cache_items_count",
		Help:      "Number of items in the cache",
	})
	cacheHitsTotal = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "cache_hits_total",
		Help:      "Total number of cache hits",
	})
	cacheMissesTotal = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "cache_misses_total",
		Help:      "Total number of cache misses",
	})
)

func init() {
	prometheus.MustRegister(
		failedRequests,
		cacheHitsTotal,
		cacheMissesTotal,
	)
}

func HandleRequestFailure(err error) {
	apiError, ok := err.(gcore.APIError)
	if !ok {
		return
	}
	code := strconv.Itoa(apiError.ResponseCode())
	failedRequests.WithLabelValues(apiError.RequestPath(), code).Inc()
}
