package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	os.Unsetenv("GCORE_ENDPOINT")
	os.Unsetenv("GCORE_API_TOKEN")
	_, err := LoadConfig()
	assert.Error(t, err)

	os.Setenv("GCORE_API_TOKEN", "secret")

	_, err = LoadConfig()
	assert.NoError(t, err)

	os.Setenv("LOG_LEVEL", "foobar")
	_, err = LoadConfig()
	assert.Error(t, err)
}

func TestConfig_String(t *testing.T) {
	c := &Config{}
	assert.NotEqual(t, "", c.String())
	c.Gcore.APIToken = "secret"
	assert.NotContains(t, c.String(), "secret")
}
