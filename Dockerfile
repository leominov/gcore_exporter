FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/github.com/leominov/gcore_exporter
COPY . .
RUN go build -o gcore_exporter ./

FROM alpine:3.11
COPY --from=builder /go/src/github.com/leominov/gcore_exporter/gcore_exporter /usr/local/bin/gcore_exporter
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["gcore_exporter"]
